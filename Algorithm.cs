﻿using System.Collections.Generic;

namespace algorithms.phonenumber
{
    public static class Algorithm
    {
        public static bool GetResultFrom(List<string> list)
        {
            if (list.Count == 0) return true;

            var result = false;
            var index = 0;
            var symbolTriCore = new SymbolTri();

            do
            {
                result = symbolTriCore.InsertSymbols(list[index]);
                index++;
            } while (result && index < list.Count);

            return result;
        }
    }
}
