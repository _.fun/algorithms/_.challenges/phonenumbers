# Phone numbers challenge

## Description

Emulate phone number input into a an old phone. There shouldn't exist a phon number that starts as already existing one. Output the result of inputing list of phone numbers as success (1), or failer (0).

## Example 1

Input:

``` C#
var list = new string[] { "0911", "0912" };
```

Output:

```C#
1
```

## Example 2

Input:

``` C#
var list = new string[] { "0911", "091"};
```

Output:

```C#
0
```