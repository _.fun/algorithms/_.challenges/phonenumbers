using System.Collections.Generic;
using Xunit;

namespace algorithms.phonenumber
{
    public class Tests
    {
        [Fact]
        public void OnePhoneNumberSuccess()
        {
            // [arrange]
            var phoneNumbers = new List<string> { "0911" };

            // [act]
            var result = Algorithm.GetResultFrom(phoneNumbers);

            // [assert]
            Assert.True(result);
        }

        [Fact]
        public void TwoPhoneNumberSuccess()
        {
            // [arrange]
            var phoneNumbers = new List<string> { "0911", "0912" };

            // [act]
            var result = Algorithm.GetResultFrom(phoneNumbers);

            // [assert]
            Assert.True(result);
        }

        [Fact]
        public void TwoPhoneNumberFailed()
        {
            // [arrange]
            var phoneNumbers = new List<string> { "0911", "091"};

            // [act]
            var result = Algorithm.GetResultFrom(phoneNumbers);

            // [assert]
            Assert.False(result);
        }
    }
}
